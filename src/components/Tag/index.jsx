import React from 'react';
import PropTypes from 'prop-types';
import memoizeOne from 'memoize-one';
import { Droppable } from 'react-beautiful-dnd';
import Room from '../RoomTile';
import './Tag.scss';
import Button from '../commons/Button';

const getSelectedMap = memoizeOne((selectedRoomsIds) => selectedRoomsIds.reduce((temp, roomId) => {
  temp[roomId] = true;
  return temp;
}, {}));

const TagColumn = (props) => {
  const {
    column, rooms, selectedRoomsIds, draggingRoomId, renderTitleGroup,
  } = props;

  const roomsCountStr = rooms.length > 1 || rooms.length === 0 ? `${rooms.length} rooms` : `${rooms.length} room`;

  return (
    <div className="tag">
      <div className="tag__header">
        {renderTitleGroup}
        <span className="tag__rooms-count">{roomsCountStr}</span>
        <div className="tag__controls-group">
          <Button onClick={props.onSelectAll}>Select All</Button>
        </div>
      </div>
      <Droppable droppableId={column.id}>
        {(provided, snapshot) => (
          <ul
            className={`tag__room-list ${snapshot.isDraggingOver ? 'tag__room-list--dragging-over' : ''}`}
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {!rooms.length && (
              <div className="tag__room-list-empty">Empty list</div>
            )}
            {rooms.map((room, index) => {
              const isSelected = Boolean(
                getSelectedMap(selectedRoomsIds)[room.roomId],
              );
              const isGhosting = isSelected
                && Boolean(draggingRoomId)
                && draggingRoomId !== room.roomId;
              return (
                <Room
                  room={room}
                  index={index}
                  key={room.roomId}
                  isSelected={isSelected}
                  isGhosting={isGhosting}
                  selectionCount={selectedRoomsIds.length}
                  toggleSelection={props.toggleSelection}
                  toggleSelectionInGroup={props.toggleSelectionInGroup}
                  multiSelectTo={props.multiSelectTo}
                />
              );
            })}
            {provided.placeholder}
          </ul>
        )}
      </Droppable>
    </div>
  );
};

TagColumn.defaultProps = {
  draggingRoomId: null,
  rooms: [],
};

TagColumn.propTypes = {
  rooms: PropTypes.array,
  selectedRoomsIds: PropTypes.array.isRequired,
  draggingRoomId: PropTypes.string,
  toggleSelection: PropTypes.func.isRequired,
  toggleSelectionInGroup: PropTypes.func.isRequired,
  multiSelectTo: PropTypes.func.isRequired,
};

export default TagColumn;
