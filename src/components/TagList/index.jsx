import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import Column from '../Tag';
import { mutliDragAwareReorder, multiSelectTo as multiSelect } from '../../utils';
import RoomService from '../../utils/RoomService';
import './TagList.scss';
import Button from '../commons/Button';
import Loader from '../commons/Loader';
import AuthContext from '../../context/AuthContext';

const FAKE_TAG_DIRECT = 'fake.direct';
const FAKE_TAG_RECENTS = 'fake.recents';

const translateTagToAdjusted = (tagName) => {
  if (tagName === 'Favourites') return 'm.favourite';
  if (tagName === 'Direct Chats') return FAKE_TAG_DIRECT;
  if (tagName === 'Default') return FAKE_TAG_RECENTS;
  if (tagName === 'Low Priority') return 'm.lowpriority';

  return tagName;
};

const translateAdjustedToTag = (adjustedTagName) => {
  if (adjustedTagName === 'm.favourite') return 'Favourites';
  if (adjustedTagName === FAKE_TAG_DIRECT) return 'Direct Chats';
  if (adjustedTagName === FAKE_TAG_RECENTS) return 'Default';
  if (adjustedTagName === 'm.lowpriority') return 'Low Priority';

  return adjustedTagName;
};

const getRooms = (entities, columnId) => entities.columns[columnId].roomsIds.map(
  (roomId) => entities.rooms[roomId],
);

export default class TagsList extends Component {
  static contextType = AuthContext;

  state = {
    entities: {},
    selectedRoomsIds: [],
    originalTags: {
      'm.favourite': {
        id: 'm.favourite',
        title: 'Favourites',
        roomsIds: [],
      },
      [FAKE_TAG_DIRECT]: {
        id: FAKE_TAG_DIRECT,
        title: 'Direct Chats',
        roomsIds: [],
      },
      [FAKE_TAG_RECENTS]: {
        id: FAKE_TAG_RECENTS,
        title: 'Default',
        roomsIds: [],
      },
      'm.lowpriority': {
        id: 'm.lowpriority',
        title: 'Low Priority',
        roomsIds: [],
      },
    },
    draggingRoomId: null,
    newTagName: '',
    loading: true,
    loadingState: {},
    tagsNames: {},
  };

  componentDidMount() {
    const { authData, logout } = this.context;
    this._logic = new RoomService(authData, logout);
    this.fetchInitialData();
    window.addEventListener('click', this.onWindowClick);
    window.addEventListener('keydown', this.onWindowKeyDown);
    window.addEventListener('touchend', this.onWindowTouchEnd);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.onWindowClick);
    window.removeEventListener('keydown', this.onWindowKeyDown);
    window.removeEventListener('touchend', this.onWindowTouchEnd);
  }

  fetchInitialData = async () => {
    this.setState({ loading: true });
    const [joinedRooms, additionalRooms] = await Promise.all([
      this._logic.getRooms(),
      this._logic.getJoinedRooms(),
    ]);

    for (const roomId of additionalRooms) {
      if (!joinedRooms[roomId]) joinedRooms[roomId] = { displayName: null, avatarMxc: null, roomId };
    }

    const tagsResponse = await this._logic.getTags();

    const originalTags = { ...this.state.originalTags };
    const tagsNames = {};
    const allRoomsArr = [];
    const roomsIdsWithExistTag = [];

    for (const tagName of Object.keys(tagsResponse)) {
      const adjustedTagName = translateAdjustedToTag(tagName);

      originalTags[tagName] = {
        id: tagName,
        title: adjustedTagName,
        roomsIds: [],
      };
      tagsNames[tagName] = tagName;

      tagsResponse[tagName].forEach((taggedRoom) => {
        let room = joinedRooms[taggedRoom.roomId];
        if (!room) room = { displayName: null, avatarMxc: null, roomId: taggedRoom.roomId };

        allRoomsArr.push(room);
        originalTags[tagName].roomsIds.push(taggedRoom.roomId);
        roomsIdsWithExistTag.push(taggedRoom.roomId);
      });
    }

    const chats = await this._logic.getDirectChats();

    const directChatRoomIds = [];
    for (const userId of Object.keys(chats)) directChatRoomIds.push(...chats[userId]);

    originalTags[FAKE_TAG_DIRECT] = this.state.originalTags[FAKE_TAG_DIRECT];

    for (const roomId of directChatRoomIds) {
      const room = joinedRooms[roomId];
      if (!room) {
        console.warn(`Ignoring unknown direct chat: ${roomId}`);
        continue;
      }
      allRoomsArr.push(room);
      originalTags[FAKE_TAG_DIRECT].roomsIds.push(roomId);
      roomsIdsWithExistTag.push(roomId);
    }

    originalTags[FAKE_TAG_RECENTS] = this.state.originalTags[FAKE_TAG_RECENTS];

    Object.keys(joinedRooms).filter((rid) => !roomsIdsWithExistTag.includes(rid)).forEach((rid) => {
      const room = joinedRooms[rid];
      allRoomsArr.push(room);
      originalTags[FAKE_TAG_RECENTS].roomsIds.push(rid);
    });

    const copyTags = {};
    const tagsOrder = [];

    // remove duplicates, needs to refactor later
    for (const tid of Object.keys(originalTags)) {
      originalTags[tid].roomsIds = [...new Set(originalTags[tid].roomsIds)];
    }

    for (const tid of Object.keys(originalTags)) {
      copyTags[tid] = {
        ...originalTags[tid],
        roomsIds: originalTags[tid].roomsIds.slice(), // clone
      };
      tagsOrder.push(tid);
    }

    const roomsMap = allRoomsArr.reduce((acc, currentRoom) => {
      acc[currentRoom.roomId] = currentRoom;
      return acc;
    }, {});

    this.setState({
      entities: {
        columns: copyTags,
        columnOrder: tagsOrder,
        rooms: roomsMap,
      },
      originalTags,
      tagsNames,
    });
    this.setState({ loading: false });
  }

  isListChanged(tagName) {
    const originalList = this.state.originalTags[tagName].roomsIds;
    const newList = this.state.entities.columns[tagName].roomsIds;

    if (!originalList || !newList) return false;

    if (originalList.filter((i) => !newList.includes(i)).length > 0) return true;
    if (newList.filter((i) => !originalList.includes(i)).length > 0) return true;

    for (const room of newList) {
      const idxA = originalList.indexOf(room);
      const idxB = newList.indexOf(room);
      if (idxA !== idxB) return true;
    }

    return false;
  }

  saveDirectChats = async () => {
    const { entities: { columns }, originalTags } = this.state;

    const knownDirectChatIds = columns[FAKE_TAG_DIRECT].roomsIds.slice();
    const legacyDirectChatIds = originalTags[FAKE_TAG_DIRECT].roomsIds
      .filter((rid) => !knownDirectChatIds.includes(rid));

    const { authData: { userId } } = this.context;

    try {
      const directChatMap = await this._logic.getDirectChats();

      // First untag all the rooms we want to untag
      console.log(`Untagging ${legacyDirectChatIds.length} rooms from direct chat map`);
      for (const memberId of Object.keys(directChatMap)) {
        const rooms = directChatMap[memberId];
        for (const roomId of legacyDirectChatIds) {
          console.log(`Removing ${roomId} from direct chats with ${memberId}`);
          const idx = rooms.indexOf(roomId);
          if (idx !== -1) rooms.splice(idx, 1);
        }
      }

      // Slot in the new tags and clean up mismatches
      console.log(`Updating ${knownDirectChatIds.length} rooms in direct chat map`);

      const promises = knownDirectChatIds.map((roomId) => this._logic.getEffectiveJoinedMembers(roomId));
      const roomsJoinedMembersByIndex = await Promise.all(promises);

      knownDirectChatIds.forEach((roomId, index) => {
        let joinedMembers = roomsJoinedMembersByIndex[index];

        // Our own user shouldn't normally appear in the direct chats
        joinedMembers = joinedMembers.filter((u) => u !== userId);

        // Special case: if we're going to end up with no members, use ourselves
        if (joinedMembers.length === 0) {
          joinedMembers = [userId];
        }

        for (const memberId of Object.keys(directChatMap)) {
          const rooms = directChatMap[memberId];
          if (rooms.includes(roomId) && !joinedMembers.includes(memberId)) {
            console.log(`Removing ${roomId} from direct chats with ${memberId} because of mismatch`);
            rooms.splice(rooms.indexOf(roomId), 1);
          } else if (!rooms.includes(roomId) && joinedMembers.includes(memberId)) {
            console.log(`Adding ${roomId} to direct chats with ${memberId}`);
            rooms.push(roomId);
          }
        }

        for (const memberId of joinedMembers) {
          let rooms = directChatMap[memberId];
          if (!rooms) {
            directChatMap[memberId] = rooms = [];
          }
          if (!rooms.includes(roomId)) {
            console.log(`Adding ${roomId} to direct chats with ${memberId} (new)`);
            rooms.push(roomId);
          }
        }
      });

      // Clean up empty entries
      const allUserIds = Object.keys(directChatMap);
      for (const memberId of allUserIds) {
        const rooms = directChatMap[memberId];
        if (!rooms || rooms.length <= 0) {
          console.log(`Removing ${memberId} from direct chats map because there are no entries`);
          delete directChatMap[memberId];
        }
      }

      console.log('Saving direct chat map...');
      await this._logic.setDirectChats(directChatMap);

      this.setState(({ entities, originalTags }) => ({
        originalTags: {
          ...originalTags,
          [FAKE_TAG_DIRECT]: {
            ...originalTags[FAKE_TAG_DIRECT],
            roomsIds: entities.columns[FAKE_TAG_DIRECT].roomsIds.slice(),
          },
        },
      }));
    } catch (e) {
      console.error('Failed to set direct chats');
      console.error(e);
      // TODO: Dialog or toast or something
    }
  }

  saveTag = async (tagName) => {
    this.setState({ loadingState: { ...this.state.loadingState, [tagName]: true } });

    const adjustedTagName = translateTagToAdjusted(tagName);
    if (adjustedTagName === FAKE_TAG_DIRECT) {
      await this.saveDirectChats();
      this.setState({ loadingState: { ...this.state.loadingState, [tagName]: false } });
      return;
    }

    // Re-scale the order so that we don't end up with infinitely small values
    const roomIds = this.state.entities.columns[tagName].roomsIds;
    const increment = 1.0 / roomIds.length;

    //  needs fix this logic later
    let isGetTagsFailed = false;
    let roomsTags = null;
    try {
      const getTagsPromises = roomIds.map((roomId) => this._logic.getTagsOnRoom(roomId));
      roomsTags = await Promise.all(getTagsPromises);
    } catch (e) {
      console.log('Failed to get tag for one of rooms. Slow mode enabled :)');
      isGetTagsFailed = true;
    }


    let isAddTagsFailed = false;
    if (adjustedTagName !== FAKE_TAG_RECENTS) {
      try {
        const addTagPromises = roomIds.map((roomId, index) => this._logic.addTagToRoom(roomId, adjustedTagName, increment * index));
        await Promise.all(addTagPromises);
      } catch (e) {
        console.log('Failed to add tag for one of rooms. Slow mode enabled :)');
        isAddTagsFailed = true;
      }
    }

    let isRemoveFagFailed = isGetTagsFailed;
    if (!isGetTagsFailed) {
      try {
        const removeTagsPromises = [];
        const idsToRemoveFromTag = {};
        roomIds.forEach((roomId, index) => {
          const toDelete = Object.keys(roomsTags[index]).filter((i) => i !== adjustedTagName);
          for (const oldTag of toDelete) {
            removeTagsPromises.push(
              this._logic.removeTagFromRoom(roomId, oldTag)
                .then(() => {
                  if (!idsToRemoveFromTag[oldTag]) idsToRemoveFromTag[oldTag] = [];
                  idsToRemoveFromTag[oldTag].push(roomId);
                }),
            );
          }
        });
        await Promise.all(removeTagsPromises);
        this.setState(({ originalTags }) => ({
          originalTags: Object.keys(originalTags).reduce((acc, tag) => {
            if (!idsToRemoveFromTag[tag]) return { ...acc, [tag]: { ...originalTags[tag] } };
            return {
              ...acc,
              [tag]: {
                ...originalTags[tag],
                roomsIds: originalTags[tag].roomsIds.filter((id) => !idsToRemoveFromTag[tag].includes(id)),
              },
            };
          }, originalTags),
        }));
      } catch (e) {
        console.log(e);
        isRemoveFagFailed = true;
        console.log('Failed to remove tag from one of rooms. Slow mode enabled :)');
      }
    }

    let index = 0;
    for (const roomId of roomIds) {
      try {
        let currentTags;
        if (isGetTagsFailed) {
          console.log(`Getting tags for ${roomId}`);
          currentTags = await this._logic.getTagsOnRoom(roomId);
        } else {
          currentTags = roomsTags[index];
          index += 1;
        }

        if (adjustedTagName !== FAKE_TAG_RECENTS && isAddTagsFailed) {
          const order = increment * roomIds.indexOf(roomId);
          console.log(`Adding '${adjustedTagName}' to ${roomId} at ${order}`);
          await this._logic.addTagToRoom(roomId, adjustedTagName, order);
        }

        const toDelete = Object.keys(currentTags).filter((i) => i !== adjustedTagName);
        console.log(`Removing ${toDelete.length} old tags from ${roomId}`);

        if (isRemoveFagFailed) {
          for (const oldTag of toDelete) {
            console.log(`Removing '${oldTag}' from ${roomId}`);
            await this._logic.removeTagFromRoom(roomId, oldTag);

            // Remove the tag from the original tag list because the user has already moved it
            this.setState(({ originalTags }) => ({
              originalTags: {
                ...originalTags,
                [oldTag]: {
                  ...originalTags[oldTag],
                  roomsIds: originalTags[oldTag].roomsIds.filter((id) => id !== roomId),
                },
              },
            }));
          }
        }
      } catch (e) {
        console.error('Failed to set tag');
        console.error(e);
        // TODO: Dialog or toast or something
      }
    }
    // Untag any rooms not currently in the list (but were previously)
    console.log(tagName);
    const oldRoomIds = this.state.originalTags[tagName].roomsIds
      .filter((rid) => !roomIds.includes(rid));
    console.log(`Untagging ${oldRoomIds.length} from old tag lists`);
    for (const oldRoomId of oldRoomIds) {
      console.log(`Untagging ${oldRoomId}`);
      this._logic.removeTagFromRoom(oldRoomId, tagName).catch((e) => {
        console.error('Failed to set tag');
        console.error(e);
      });
    }
    this.setState(({ entities, originalTags }) => ({
      originalTags: {
        ...originalTags,
        [tagName]: {
          ...originalTags[tagName],
          roomsIds: entities.columns[tagName].roomsIds.slice(),
        },
      },
      loadingState: { ...this.state.loadingState, [tagName]: false },
    }));
  }

  // TODO
  onDragStart = (start) => {
    const id = start.draggableId;
    const selected = this.state.selectedRoomsIds.find(
      (roomId) => roomId === id,
    );

    // if dragging an item that is not selected - unselect all items
    if (!selected) {
      this.unselectAll();
    }
    this.setState({
      draggingRoomId: start.draggableId,
    });
  };

  onDragEnd = (result) => {
    const { destination } = result;
    const { source } = result;
    // nothing to do
    if (!destination || result.reason === 'CANCEL') {
      this.setState({
        draggingRoomId: null,
      });
      return;
    }

    const processed = mutliDragAwareReorder({
      entities: this.state.entities,
      selectedRoomsIds: this.state.selectedRoomsIds,
      source,
      destination,
    });

    this.setState({
      ...processed,
      draggingRoomId: null,
    });
  };

  onWindowKeyDown = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    if (event.key === 'Escape') {
      this.unselectAll();
    }
  };

  onWindowClick = (event) => {
    if (event.defaultPrevented) {
      return;
    }
    this.unselectAll();
  };

  onWindowTouchEnd = (event) => {
    if (event.defaultPrevented) {
      return;
    }
    this.unselectAll();
  };

  toggleSelection = (roomId) => {
    const { selectedRoomsIds } = this.state;
    const wasSelected = selectedRoomsIds.includes(roomId);

    const newRoomsIds = (() => {
      // Room was not previously selected
      // now will be the only selected item
      if (!wasSelected) {
        return [roomId];
      }

      // Room was part of a selected group
      // will now become the only selected item
      if (selectedRoomsIds.length > 1) {
        return [roomId];
      }

      // room was previously selected but not in a group
      // we will now clear the selection
      return [];
    })();

    this.setState({
      selectedRoomsIds: newRoomsIds,
    });
  };

  toggleSelectionInGroup = (roomId) => {
    const { selectedRoomsIds } = this.state;
    const index = selectedRoomsIds.indexOf(roomId);

    // if not selected - add it to the selected items
    if (index === -1) {
      this.setState({
        selectedRoomsIds: [...selectedRoomsIds, roomId],
      });
      return;
    }

    // it was previously selected and now needs to be removed from the group
    const shallowRoomsIds = [...selectedRoomsIds];
    shallowRoomsIds.splice(index, 1);
    this.setState({
      selectedRoomsIds: shallowRoomsIds,
    });
  };

  // This behaviour matches the MacOSX finder selection
  multiSelectTo = (newRoomId) => {
    const updated = multiSelect(
      this.state.entities,
      this.state.selectedRoomsIds,
      newRoomId,
    );

    if (updated == null) {
      return;
    }

    this.setState({
      selectedRoomsIds: updated,
    });
  };

  unselectAll = () => {
    this.setState({
      selectedRoomsIds: [],
    });
  };

   handleSaveTag = (tagId) => async (event) => {
     event.preventDefault();
     await this.saveTag(tagId);
   }

  handleSelectAll = (tagId) => (event) => {
    event.preventDefault();
    this.setState(({ entities }) => ({
      selectedRoomsIds: entities.columns[tagId].roomsIds.slice(),
    }));
  }

  handleAddNewTag = (event) => {
    event.preventDefault();
    this.setState((prevState) => {
      const tagName = prevState.newTagName;
      return {
        tagsNames: { ...prevState.tagsNames, [tagName]: tagName },
        newTagName: '',
        entities: {
          ...prevState.entities,
          columns: {
            ...prevState.entities.columns,
            [tagName]: {
              id: tagName,
              title: tagName,
              roomsIds: [],
            },
          },
          columnOrder: [
            ...prevState.entities.columnOrder,
            tagName,
          ],
        },
        originalTags: {
          ...prevState.originalTags,
          [tagName]: {
            id: tagName,
            title: tagName,
            roomsIds: [],
          },
        },
      };
    });
  }

  handleTagNameInput = (oldTagName) => (event) => {
    event.preventDefault();
    const { value } = event.target;
    if (!oldTagName) {
      this.setState({ newTagName: value });
      return;
    }

    this.setState(({ tagsNames }) => ({
      tagsNames: { ...tagsNames, [oldTagName]: value },
    }));
  };

  handleRenameTag = (oldName) => (event) => {
    event.preventDefault();

    const newName = this.state.tagsNames[oldName];

    this.setState((prevState) => {
      const { tagsNames, entities: { columns, columnOrder }, originalTags } = prevState;


      const newTagsNames = Object.keys(tagsNames).reduce((acc, key) => {
        if (key === oldName) return { ...acc, [newName]: newName };
        return { ...acc, [key]: tagsNames[key] };
      }, {});

      const newColumns = {};
      for (const tagId of Object.keys(columns)) {
        if (tagId !== oldName) {
          newColumns[tagId] = columns[tagId];
          continue;
        }
        newColumns[newName] = {
          id: newName,
          title: newName,
          roomsIds: columns[oldName].roomsIds,
        };
      }

      const newOriginalTags = {};
      for (const tagId of Object.keys(originalTags)) {
        if (tagId !== oldName) {
          newOriginalTags[tagId] = originalTags[tagId];
          continue;
        }
        newOriginalTags[newName] = {
          id: newName,
          title: newName,
          roomsIds: originalTags[oldName].roomsIds,
        };
      }

      const newColumnOrder = columnOrder.map((tagName) => (tagName === oldName ? newName : tagName));

      return {
        tagsNames: newTagsNames,
        entities: {
          ...prevState.entities,
          columns: newColumns,
          columnOrder: newColumnOrder,
        },
        originalTags: newOriginalTags,
      };
    }, () => this.saveTag(newName));
  }

  renderTitleGroup = (tagId) => {
    const { tagsNames, originalTags, loadingState } = this.state;
    const tagName = originalTags[tagId].title;

    const isCustomTag = tagName === tagId;

    if (isCustomTag) {
      let saveButton = (this.isListChanged(tagId) || loadingState[tagId]) && <Button loading={loadingState[tagId]} onClick={this.handleSaveTag(tagId)}>Save</Button>;
      const isTagNameChanged = tagsNames[tagId] !== tagId;

      if (isTagNameChanged) {
        saveButton = <Button loading={loadingState[tagId]} onClick={this.handleRenameTag(tagId)}>Save</Button>;
      }

      return (
        <div className="tag__title-group">
          <input
            className="tag__title tag__input"
            value={tagsNames[tagName]}
            onChange={this.handleTagNameInput(tagName)}
          />
          {saveButton}
        </div>
      );
    }

    return (
      <div className="tag__title-group">
        <h4 className="tag__title">{tagName}</h4>
        {this.isListChanged(tagId) && <Button loading={loadingState[tagId]} onClick={this.handleSaveTag(tagId)}>Save</Button>}
      </div>
    );
  }

  renderNewTagCol = () => {
    const { newTagName, entities: { columns } } = this.state;
    const tagIds = Object.keys(columns);
    const disabled = !newTagName.length || tagIds.find((tagId) => columns[tagId].title === newTagName);
    return (
      <li className="tag-list__item" key="new-tag-col">
        <div className="tag">
          <div className="tag__header">
            <div className="tag__title-group">
              <input className="tag__title tag__input tag__input--visible" onChange={this.handleTagNameInput()} value={newTagName} />
              <Button disabled={disabled} onClick={this.handleAddNewTag}>Add</Button>
            </div>
            <span className="tag__rooms-count"> </span>
            <div className="tag__controls-group" />
          </div>
          <ul className="tag__room-list" />
        </div>
      </li>
    );
  }

  render() {
    const { loading, entities, selectedRoomsIds } = this.state;

    if (loading) {
      return (
        <div className="centered">
          <Loader size="large" />
        </div>
      );
    }

    return (
      <div className="wrapper">
        <DragDropContext
          onDragStart={this.onDragStart}
          onDragEnd={this.onDragEnd}
        >
          <ul className="tag-list">
            {entities.columnOrder.map((columnId) => (
              <li className="tag-list__item" key={columnId}>
                <Column
                  column={entities.columns[columnId]}
                  rooms={getRooms(entities, columnId)}
                  selectedRoomsIds={selectedRoomsIds}
                  draggingRoomId={this.state.draggingRoomId}
                  toggleSelection={this.toggleSelection}
                  toggleSelectionInGroup={this.toggleSelectionInGroup}
                  multiSelectTo={this.multiSelectTo}
                  onSelectAll={this.handleSelectAll(columnId)}
                  renderTitleGroup={this.renderTitleGroup(columnId)}
                />
              </li>
            ))}
            {this.renderNewTagCol()}
          </ul>
        </DragDropContext>
      </div>
    );
  }
}
