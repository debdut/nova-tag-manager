import React from 'react';
import PropTypes from 'prop-types';
import './Loader.scss';

const Loader = ({ size }) => <span className={`loader loader--${size}`} />;

Loader.defaultProps = {
  size: 'small',
};

Loader.propTypes = {
  size: PropTypes.string,
};

export default Loader;
