import React from 'react';

const AuthContext = React.createContext({
  authData: {},
  onLogin: () => {},
  onLogout: () => {},
});

AuthContext.displayName = 'AuthContext';

export default AuthContext;
