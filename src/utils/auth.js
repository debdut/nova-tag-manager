import axios from 'axios';

export const login = async ({ hsUrl, username, password }) => {
  const res = await axios.post(`${hsUrl}/_matrix/client/r0/login`, {
    type: 'm.login.password',
    identifier: {
      type: 'm.id.user',
      user: username,
    },
    password,
    initial_device_display_name: 'matrix-tag-manager',
  });
  return res.data;
};

export const logout = async ({ hsUrl, accessToken }) => {
  await axios.post(`${hsUrl}/_matrix/client/r0/logout`, {}, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
};

export const getUserDisplayName = async ({ hsUrl, userId, accessToken }) => {
  const { data } = await axios.get(`${hsUrl}/_matrix/client/r0/profile/${userId}/displayname`, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
  return data.displayname;
};
